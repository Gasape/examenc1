const llamandoFetch=()=>{
    const url = "../HTML/servicio.json";

    fetch(url).then(respuesta =>respuesta.json()).then(datos => mostrarDatos(datos))
    .catch((reject)=>{
        console.log("Ocurrio un error");
    })
    const mostrarDatos=(data)=>{

        const res = document.getElementById("tabla");
        
        const gananciaS = document.getElementById('Ganancia');
        let ganancia = 0
        let gananciaT = 0
        let par = 0
        let contador = 1
        for(let item of data){
            ganancia = (item.preciovta * item.preciocompra) * item.cantidad;
            contador = contador + 1;
            par = contador % 2;
            if(par==0){
                res.innerHTML += "<tr class='r1'><td>" + item.codigo + "</td><td>" + item.idcliente + "</td><td>" + item.descripcion + "</td><td>" + item.cantidad + "</td><td>" + item.preciovta + "</td><td>" + item.preciocompra + "</td><td> "+ ganancia +"</td></tr>";
            }else{
                res.innerHTML += "<tr class='r2'><td>" + item.codigo + "</td><td>" + item.idcliente + "</td><td>" + item.descripcion + "</td><td>" + item.cantidad + "</td><td>" + item.preciovta + "</td><td>" + item.preciocompra + "</td><td> "+ ganancia +"</td></tr>";
            }
            
            gananciaT = gananciaT + ganancia;
            gananciaS.innerHTML = "Ganancia Total: " +gananciaT;
        }

    }
}

document.getElementById('btnCargar').addEventListener('click', function() {
    llamandoFetch();
})